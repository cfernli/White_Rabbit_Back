package com.Itau.WhiteRabbit.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Itau.WhiteRabbit.model.Usuario;
import com.Itau.WhiteRabbit.repository.UsuarioRepository;
import com.Itau.WhiteRabbit.service.PasswordService;
import com.Itau.WhiteRabbit.service.TokenService;


@Controller
public class UsuarioController {
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	PasswordService passwordService;
	
	TokenService tokenService = new TokenService();
	
	@CrossOrigin
	@RequestMapping(path="/cadastrar/usuario", method=RequestMethod.POST)
	@ResponseBody
	public Usuario inserirUsuario(@RequestBody Usuario usuario) {
		String hash = passwordService.encode(usuario.getSenha());
		usuario.setSenha(hash);
		return usuarioRepository.save(usuario);
	}
	
	@CrossOrigin
	@RequestMapping(path="/login", method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> logar(@RequestBody Usuario usuario) {
		Optional<Usuario> usuarioBanco = usuarioRepository.findById(usuario.getFuncional());	
		
		if(! usuarioBanco.isPresent()) {
			return ResponseEntity.badRequest().build();
		}

		boolean deuCerto = passwordService.verificar(usuario.getSenha(), usuarioBanco.get().getSenha());
		
		if(deuCerto) {
			String token = tokenService.gerar(usuarioBanco.get().getFuncional());
			
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", String.format("Bearer %s", token));
			headers.add("Access-Control-Expose-Headers", "Authorization");
			return new ResponseEntity<Usuario>(usuarioBanco.get(), headers, HttpStatus.OK);
		}
		
		return ResponseEntity.badRequest().build();
	}
}
