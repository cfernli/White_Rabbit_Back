package com.Itau.WhiteRabbit.controller;

import java.text.SimpleDateFormat;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Itau.WhiteRabbit.model.Atividade;
import com.Itau.WhiteRabbit.model.Usuario;
import com.Itau.WhiteRabbit.repository.AtividadeRepository;
import com.Itau.WhiteRabbit.repository.UsuarioRepository;
import com.Itau.WhiteRabbit.service.TokenService;

@Controller
public class AtividadeController {
	
	@Autowired
	AtividadeRepository atividadeRepository;
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	TokenService tokenService;
	
	@CrossOrigin
	@RequestMapping(path="/atividades", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Atividade> getVariasAtividades() {		
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		int id = 1;
//		Date hora = Calendar.getInstance().getTime(); // Ou qualquer outra forma que tem
		Atividade atividadeHora = atividadeRepository.findById(id);
		
		String horaFormatada = sdf.format(atividadeHora.getHoraFim());
		System.err.println("hora banco: " + atividadeHora.getHoraFim());
		System.err.println("hora formatada: " + horaFormatada);
		
		return atividadeRepository.findAll();
	}
	
	@CrossOrigin
	@RequestMapping(path="/cadastrarAtividade", method=RequestMethod.POST)
	public ResponseEntity<?> cadastrarAtividade(HttpServletRequest request, @RequestBody Atividade atividade) {
		
		
		String bearer = request.getHeader("Authorization");
		
		String token = bearer.replace("Bearer ", "");
		
		String idUsuario = tokenService.verificar(token);
		Optional<Usuario> usuarioBanco = usuarioRepository.findById(idUsuario);
		
		if (usuarioBanco.isPresent()){
			Usuario usuario = new Usuario();
			usuario.setFuncional(idUsuario);
			atividade.setUsuario(usuario);
			atividade = atividadeRepository.save(atividade);
			return ResponseEntity.ok(atividade);
		}
		
		return ResponseEntity.badRequest().body("Atividade não cadastrada, usuário não encontrado.");	
		
	}
	
	@CrossOrigin
	@RequestMapping(path="/excluirAtividade", method=RequestMethod.POST)
	public ResponseEntity<?>  excluirAtividade(HttpServletRequest request, @RequestBody Atividade atividade) {
		String bearer = request.getHeader("Authorization");
		String token = bearer.replace("Bearer ", "");
		
		String idUsuario = tokenService.verificar(token);
        Optional<Usuario> usuarioBanco = usuarioRepository.findById(idUsuario);
		
		if (usuarioBanco.isPresent()){
			atividadeRepository.delete(atividade);
			return ResponseEntity.ok(atividade);
		}
		
    	return ResponseEntity.badRequest().body("Atividade não excluída, usuário não encontrado.");	
	
	}

	@CrossOrigin
	@RequestMapping(path="/consultaAtividadesDia/{data}", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Atividade> getAtividadesDia(HttpServletRequest request, @PathVariable int data) {		
		String bearer = request.getHeader("Authorization");
		String token = bearer.replace("Bearer ", "");
		
		String idUsuario = tokenService.verificar(token);
		Optional<Usuario> usuarioBanco = usuarioRepository.findById(idUsuario);
	
		if (usuarioBanco.isPresent()){
			return atividadeRepository.findByUsuarioAndDataOrderByHoraInicio(usuarioBanco, data);
		}
		return null;
	}

	@RequestMapping(path="/consultaAtividadesMes/{dataIni}", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Atividade> getAtividadesMes(HttpServletRequest request, @PathVariable int dataIni) {		
		String bearer = request.getHeader("Authorization");
		String token = bearer.replace("Bearer ", "");
	
		String idUsuario = tokenService.verificar(token);
		Optional<Usuario> usuarioBanco = usuarioRepository.findById(idUsuario);
	
		if (usuarioBanco.isPresent()){
			int dataFim = dataIni + 30;
			return atividadeRepository.findByUsuarioAndDataBetweenOrderByData(usuarioBanco, dataIni, dataFim);
		}
		return null;
		
	}
}
