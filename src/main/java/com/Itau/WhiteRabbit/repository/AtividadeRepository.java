package com.Itau.WhiteRabbit.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.Itau.WhiteRabbit.model.Atividade;
import com.Itau.WhiteRabbit.model.Usuario;

public interface AtividadeRepository extends CrudRepository<Atividade, Integer> {
		Atividade findById(int id);
		
		Iterable<Atividade> findByData(int data);

		Iterable<Atividade> findByUsuarioAndDataOrderByHoraInicio(Optional<Usuario> usuario, int data);

//		Iterable<Atividade> findByUsuarioAndDataBetweenOrderByData(Optional<Usuario> usuario, int dataIni, int dataFim);

		Iterable<Atividade> findByUsuarioAndDataBetweenOrderByData(Optional<Usuario> usuario, int dataIni, int dataFim);

}